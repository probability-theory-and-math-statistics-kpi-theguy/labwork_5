package com.kpi;

import java.util.Random;

/**
 * Class of experimental part of task is calculating, or where we have an model of process of Markov
 * @author Yuriy
 * @verison 1.05
 */
public class ExperimentalMatrix {
    private double[][] matrix1;
    private double[][] matrix2;
    private double[] states;
    private int times;

    /**
     * Getter of the first matrix, main matrix setted in main method
     * @return
     * A value from a field, that saved there
     */
    public double[][] getMatrix1() {
        return matrix1;
    }

    /**
     * Setter of a first matrix, which we taked from main class
     * @param matrix1
     * A matrix, that will be saved in a field of class
     */
    public void setMatrix1(double[][] matrix1) {
        this.matrix1 = matrix1;
    }

    /**
     * Getter of the second matrix, modified by summing an elements of first
     * @return
     * A value from a field, that saved there
     */
    public double[][] getMatrix2() {
        return matrix2;
    }

    /**
     * Setter of a second matrix, which we create there using a first matrix
     * @param matrix2
     * A matrix, that will be saved in a field of class
     */
    public void setMatrix2(double[][] matrix2) {
        this.matrix2 = matrix2;
    }

    /**
     * Getter of states array, array of values that comes an system during the process
     * @return
     * A value from this field about this thing
     */
    public double[] getStates() {
        return states;
    }

    /**
     * Setter of a states array, which consist how many a peaks come an system
     * @param states
     * An array of states, which will be saved in this class
     */
    public void setStates(double[] states) {
        this.states = states;
    }

    /**
     * Getter of times, number of iteration of all process
     * @return
     * A value which is saved in a field of class
     */
    public int getTimes() {
        return times;
    }

    /**
     * Setter of times, a value of iteration of a model
     * @param times
     * A number, of how many time will be process running
     */
    public void setTimes(int times) {
        this.times = times;
    }

    /**
     * Constructor of a class, which sets a first values into a field
     * @param enterMatrix
     * A value of starting matrix
     * @param n
     * A value of number of itteration
     */
    public ExperimentalMatrix(double[][] enterMatrix, int n){
        setMatrix1(enterMatrix);
        setTimes(n);
    }

    /**
     * A method of generating of modified matrix, made from first matrix
     */
    public void generateModifiedMatrix(){
        double[][] buffMatrix = getMatrix1();
        double[][] newMatrix = new double[buffMatrix.length][buffMatrix.length];
        double sum = 0;
        for (int i = 0; i < buffMatrix.length; i++){
            for (int j = 0; j < buffMatrix[i].length; j++){
                sum += buffMatrix[i][j];
                newMatrix[i][j] = sum;
            }
            sum = 0;
        }
        setMatrix2(newMatrix);
    }

    /**
     * Method, of setting States, using a modified matrix
     */
    public void setStating(){
        generateModifiedMatrix();
        double[][] buffMatrix = getMatrix2();
        double[] buffVector = new double[buffMatrix.length];
        double r = 0;
        int t = 1;
        Random rand = new Random(System.currentTimeMillis());
        for (int i = 0; i < getTimes(); i++){
            r = rand.nextDouble();
            for (int j = 0; j < buffMatrix.length; j++){
                if (r <= buffMatrix[t][j]){
                    buffVector[t]++;
                    t = j;
                    break;
                }
            }
        }
        setStates(buffVector);
    }

    /**
     * Method, that generates how often a peak was visited
     * @return
     * An array of values of each peak, visited by a state
     */
    public double[] generateStating(){
        setStating();
        double[] buffVector = getStates();
        double[] resultVector = new double[buffVector.length];
        for (int i = 0; i < resultVector.length; i++){
            resultVector[i] = (buffVector[i] / getTimes());
        }
        return resultVector;
    }
}
