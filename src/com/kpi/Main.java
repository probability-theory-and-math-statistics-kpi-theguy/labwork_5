package com.kpi;

import java.util.*;
import java.io.*;
import static java.lang.System.*;

/**
 * Main class of creating a process of Markov
 * @author Yuriy
 * @version 1.05
 */

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(in);
        out.println("We have discrete process of Markov. Enter the number of peaks:");
        int p = scan.nextInt();
        out.println("How many times we will go throw this process of Markov?");
        int n = scan.nextInt();
        out.println("Would you like to enter the matrix, or fill it by random numbers?:");
        out.println("1. I want enter the matrix by my own.");
        out.println("2. I want that matrix will generate randomly.");
        int ch = scan.nextInt();
        double[][] stateMatrix = new double[p][p];
        if (ch == 1){
            out.println("Let's fill the matrix:");
            for (int i = 0; i < p; i++){
                for (int j = 0; j < p; j++){
                    out.print("Matrix ["+i+" , "+j+"] =");
                    stateMatrix[i][j] = scan.nextDouble();
                }
                out.println();
            }
        }
        else{
            out.println("Matrix will be generated randomly numbers.");
            double sum = 0;
            for (int i = 0; i < p; i++){
                for (int j = 0; j < p; j++){
                    if (j == 0){
                        stateMatrix[i][j] = generateRandom(0.5);
                    }
                    else if (j == p - 1){
                        stateMatrix[i][j] = 1 - sum;
                    }
                    else{
                        stateMatrix[i][j] = generateRandom(1 - sum);
                    }
                    if (sum < 1.0) {
                        sum += stateMatrix[i][j];
                    }
                    else if (sum > 1.0){
                        stateMatrix[i][j] = stateMatrix[i][j] - (sum - 1);
                        sum = 1.0;
                    }
                }
                sum = 0;
            }
        }
        out.println("So, we have this matrix of probabilities:");
        matrixDisplay(stateMatrix);
        SolvingMatrix matr = new SolvingMatrix(stateMatrix);
        out.println("The result of solving a static probabilities system is:");
        double[] resultVector = matr.solvingSystem();
        for (int i = 0; i < resultVector.length; i++){
            out.println("P ("+ (i + 1) +") = "+ resultVector[i]);
        }
        ExperimentalMatrix matr2 = new ExperimentalMatrix(stateMatrix, n);
        out.println("Modified matrix of probabilities to go:");
        matr2.generateModifiedMatrix();
        matrixDisplay(matr2.getMatrix2());
        out.println("Our matrix with size of "+p+" is saved in file: С://generator4.txt");
        File fil = new File("C://generator4.txt");
        try {
            if(!fil.exists()){
                fil.createNewFile();
            }
            BufferedWriter buff = new BufferedWriter(new FileWriter(fil));
            for(int i = 0; i < stateMatrix.length; i++){
                for (int j = 0; j < stateMatrix[i].length; j++) {
                    buff.write(stateMatrix[i][j] + " ");
                }
                buff.write("\n");
            }
            buff.write("         2: ");
            for(int i = 0; i < matr2.getMatrix2().length; i++){
                for (int j = 0; j < matr2.getMatrix2()[i].length; j++) {
                    buff.write(matr2.getMatrix2()[i][j] + " ");
                }
                buff.write("\n");
            }
            buff.flush();
            buff.close();
        }catch(IOException e){
            err.println(e.getMessage());
        }
        double[] statingVector = matr2.generateStating();
        out.println("In this iteration, this process was in this peaks for "+n+" entered time:");
        for (int i = 0; i < statingVector.length; i++){
            out.println("Peak no. "+ (i + 1) +" = "+statingVector[i]);
        }
    }

    /**
     * Method, that makes a matrix to show it on the console
     * @param matrix
     * Matrix, that we are going to show on the console
     */
    public static void matrixDisplay(double[][] matrix){
        for (int i = 0; i < matrix.length; i++){
            for (int j = 0; j < matrix[i].length; j++){
                out.print(matrix[i][j]+"  ");
            }
            out.println();
        }
    }

    /**
     * Method, that generates a random value from interval (0;l]
     * @param l
     * A right interval of generating, which is sets a method
     * @return
     * A random double value from this interval
     */
    public static double generateRandom (double l){
        return Math.random()*l;
    }
}
