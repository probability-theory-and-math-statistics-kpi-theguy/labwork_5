package com.kpi;

import org.apache.commons.math3.linear.*;
/**
 * Class of Solving matrix, which solves a system of linear equations
 * @author Yuriy
 * @version 1.05
 */
public class SolvingMatrix {
    private double[][] matrix1;
    private double[][] SLEmatrix;
    private double[] SLEvector;

    /**
     * Getter of main matrix, that have a value of process
     * @return
     * A value which is saved in this field of matrix
     */
    public double[][] getMatrix1() {
        return matrix1;
    }

    /**
     * Setter of main matrix, which sets a value of matrix of process of Markov
     * @param matrix1
     * A matrix, that we want to save in the field
     */
    public void setMatrix1(double[][] matrix1) {
        this.matrix1 = matrix1;
    }

    /**
     * Getter of SLE matrix, which forms a system of linear equations
     * @return
     * A value which is saved in this field of matrix
     */
    public double[][] getSLEmatrix() {
        return SLEmatrix;
    }

    /**
     * Setter of SLE matrix, which sets a value of matrix of systen of linear equations
     * @param SLAEmatrix
     * A matrix, that we want to save in the field
     */
    public void setSLEmatrix(double[][] SLAEmatrix) {
        this.SLEmatrix = SLAEmatrix;
    }

    /**
     * Getter of SLE vector, where we have a value of right side of system
     * @return
     * A Value of vector, whic is saved in a field
     */
    public double[] getSLEvector() {
        return SLEvector;
    }

    /**
     * Setter of SLE vector, which we want to save in a field of class
     * @param SLAEvector
     * A vector of right side, that is about SLE
     */
    public void setSLEvector(double[] SLAEvector) {
        this.SLEvector = SLAEvector;
    }

    /**
     * Constructor of class of SolvingMatrix, that sets a starting value into a field
     * @param entermatrix
     * A main matrix, formed in a main class
     */
    public SolvingMatrix(double[][] entermatrix){
        setMatrix1(entermatrix);
    }

    /**
     * Method of generating of Matrix Structure, to solve a system of linear equation
     */
    public void generateMatrixStructure (){
        double[][] localMatrix = getMatrix1();
        double[][] buffMatr = new double[localMatrix.length][localMatrix.length];
        double[] buffVect = new double[localMatrix.length];
        for (int i = 0; i < localMatrix.length; i++){
            buffMatr[0][i] = 1;
            buffVect[0] = 1;
        }
        for (int i = 1; i < localMatrix.length; i++){
            for (int j = 0; j < localMatrix.length; j++){
                if (i == j) {
                    for (int p = 0; p < localMatrix.length; p++) {
                        if (p != j) {
                            buffMatr[i][j] -= localMatrix[j][p];
                        }
                    }
                }
                else{
                    buffMatr[i][j] = localMatrix[j][i];
                }
            }
            buffVect[i] = 0;
        }
        setSLEmatrix(buffMatr);
        setSLEvector(buffVect);
    }

    /**
     * Method, where a specific class of apachee math3 solves an system of linear equation
     * @return
     * A vector of solved linear equation
     */
    public double[] solvingSystem (){
        generateMatrixStructure();
        RealMatrix coefficients = new Array2DRowRealMatrix(getSLEmatrix(), false);
        DecompositionSolver solve = new LUDecomposition(coefficients).getSolver();
        RealVector constants = new ArrayRealVector(getSLEvector(), false);
        RealVector solution = solve.solve(constants);
        double[] solutionArray = new double[getMatrix1().length];
        for (int i = 0; i < solutionArray.length; i++){
            solutionArray[i] = solution.getEntry(i);
        }
        return solutionArray;
    }
}
